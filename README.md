# 基于layui的thinkphp5分页驱动

基于layui的thinkphp5分页驱动

## 安装

```
composer require jayfun/layui-pager dev-master
```

## 使用方法

安装成功后修改thinkphp5配置文件`config.php`最底部的分页配置的type项

```
    //分页配置
    'paginate'               => [
        'type'      => 'jayfun\layuiPager\Layui',
        'var_page'  => 'page',
        'list_rows' => 3,
    ],
```

大工告成，在layui为前端框架的模版中直接可以渲染出layui的分页按钮！！

喜欢请star